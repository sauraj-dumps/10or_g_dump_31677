## msm8953_64-userdebug 7.1.2 N2G47H 7136 release-keys
- Manufacturer: 10or
- Platform: msm8953
- Codename: G
- Brand: 10or
- Flavor: msm8953_64-userdebug
- Release Version: 7.1.2
- Id: N2G47H
- Incremental: 7136
- Tags: release-keys
- CPU Abilist: arm64-v8a,armeabi-v7a,armeabi
- A/B Device: false
- Locale: en-IN
- Screen Density: undefined
- Fingerprint: 10or/G/G:7.1.2/N2G47H/7136:userdebug/release-keys
- OTA version: 
- Branch: msm8953_64-userdebug-7.1.2-N2G47H-7136-release-keys
- Repo: 10or_g_dump_31677


>Dumped by [Phoenix Firmware Dumper](https://github.com/DroidDumps/phoenix_firmware_dumper)
